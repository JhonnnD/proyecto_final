<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="/CSS/Style_index.css">
    <!--<link rel="stylesheet" href="/bootstrap-5.1.1-dist/css/bootstrap-grid.min.css">-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Green Dragon Hotel</title>
  </head>
  <body>
    <!--La cabesera-->
    <header class="container-fluid bg-success d-flex justify-content-center">
        <p class="text-light mb-0 p-2 fs-6"> Green Dragon Hotel </p>
    </header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light p-3" id="menu">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">
          <span class="text-success fs-5 fw-blod">Green Dragon Hotel </span>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">

            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="C:\Users\pc\Proyectos\Proyecto_Final\pag_register.html">Inicio</a>
            </li> 

            <li class="nav-item">
              <a class="nav-link" href="#equipo">Equipo</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#seccion-contacto">Contactos</a>
            </li>
           
          </ul>
          <form class="d-flex">
            <input class="form-control me-2" type="email" placeholder="Correo electronico" aria-label="email">
            <button class="btn btn-success" type="submit">Suscribite</button>
          </form>
        </div>
      </div>
    </nav>

    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="/Imagenes/imagen1.jpg" class="d-block w-200" alt="imagen1">
        </div>

        <div class="carousel-item">
          <img src="/Imagenes/imagen2.jpg" class="d-block w-200" alt="imagen2">
        </div>

        <div class="carousel-item">
          <img src="/Imagenes/imagen3.jpg" class="d-block w-200" alt="imagen3">
        </div>

      </div>
      <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>
    </div>

    <section class="w-50 mx-auto text-center" id="intrp"> <!--para centrara -->
      <h1 class="p-3 fs-2 border-top border-3"> Hotel Drgon Verde <samp class="text-primary"> donde sus sueños se acen realidad</samp></h1>
      <p class="p-3 fs"><samp class="text-primary"> Hotel Dragon Verde</samp>es una estadia para todas las persona que quieren sentirse como un rey, por que nuestras 
        instalaciones son comodas y acojedoras con justos diferentes para cada diferente huesped</p>
    </section>

    <section class="container-fluid">
      <div class="row w-75 mx-auto servicio-fila">

        <div class="col-lg-6 col-md-12 col-sm-12
        d-flex my-5 icono-wrap ">
          <img src="/Imagenes/undraw_Confirmed_re_sef7.svg" alt="inscripcion" 
          width="180" height="160">
        </div>
        <div>
          <h3 class="fs-5 mt-4 px-3 pb-1" >Habitaciones acojedoras</h3>
          <p class="px-4"> habitaciones matrimoniales indibiduales duplicas y con servicios a cuartos</p>
        </div>

        <div class="col-lg-6 col-md-12 col-sm-12
        d-flex my-5 icono-wrap ">
          <img src="/Imagenes/undraw_Map_dark_re_36sy.svg" alt="inscripcion" 
          width="180" height="160">
        </div>
        <div>
          <h3 class="fs-5 mt-4 px-3 pb-1" >Con sucursales por todo el mundo</h3>
          <p class="px-4">Te perdiste y no saves que hacer puees no te preocupes
             por que ahora gracias a ustedes somos sede internacional en el hambito de hoteleria y resepcion</p>
        </div>

        <div class="col-lg-6 col-md-12 col-sm-12
        d-flex my-5 icono-wrap ">
          <img src="/Imagenes/undraw_Meditation_re_gll0.svg" alt="inscripcion" 
          width="180" height="160">
        </div>
        <div>
          <h3 class="fs-5 mt-4 px-3 pb-1" >Con Hambientes relagantes</h3>
          <p class="px-4"> La monotonia de la ciudad te estresa, no hay problema por que nuestros 
            resintos cuentan con sentros de meditacion con maestros especialisados en el tema àra que te relajes </p>
        </div>
        
        <div class="col-lg-6 col-md-12 col-sm-12
        d-flex my-5 icono-wrap ">
          <img src="/Imagenes/undraw_Events_re_98ue.svg" alt="inscripcion" 
          width="180" height="160">
        </div>
        <div>
          <h3 class="fs-5 mt-4 px-3 pb-1" >Eventos Ocacionales</h3>
          <p class="px-4">Te cuesta convivir con jente a tu alrededor, ya no sera un problema por que nosotros
            creamos eventos recreativos personalisados programados todo para que empises a socialisar con las personas </p>
        </div>

      </div>
    </section>

    <section>
      <div id="equipo" class="w-75 m-auto text-center">
        <h1 class="mb-5 fs-2">Has tus reservas <samp class="text-primary">ya para no quedarte fuera</samp></h1>
        <p class="fs-4">Tenemos habitaciones de todo tipo que te puedas imaginar desde las mas economicas hasta las submit
          condiferentes estilos personalisados .Si reservas de antemano podemos personalisar tu habitacion a tu gusto con las descabelladas opciones que se te ocuran con un 
          modico precio extra a tu saldo final</p> 
      </div>
      <div class="mt-5 text-center">
        <img class="img-fluid" src="/Imagenes/undraw_Travel_booking_re_6umu.svg" alt="equipo">
      </div>


      <div id="local" class="border-top">
        <div class="fondo"></div>
        <div>
          <div class="wrapper">
            <h2>Ven a pasar tus vacaciones con nosotros </h2>
            <h2 class="text-success mb-4" id="typewriter"></h2>
            <p class="fs-5 text-body"> Si eres unos de las personas afortunadas en ganr el premio mayor del hotel tienes a habilidad de recibir 
              jugosos premios ya sea en descuentos rebajas del hotel o incluso una estadia gratis "Promocion habilitada solo para los Aniversarios del Hotel "
            </p>
            <section class="d-flex" id="numeros_local">
              <div>
                <p class="text-success fs-5">Descuaentos del</p>
                <p>25%</p>
              </div>

              <div>
                <p class="text-prymary fs-5">Descuento por Parejas del</p>
                <p>50%</p>
              </div>

              <div>
                <p class="text-success fs-5">Bufet de barra libre para las Personas de Tercera edad</p>
                <p>Total mente GRATIS</p>
              </div>
            </section>
          </div>
        </div>
      </div>
    </section>

    <section id="seccion-contacto" class="border-bottom border-secondary border-2">
      <div id="bg-contacto">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#00cba9" fill-opacity="1" d="M0,128L80,32L160,0L240,160L320,96L400,288L480,288L560,
          320L640,288L720,160L800,128L880,96L960,128L1040,224L1120,96L1200,288L1280,288L1360,96L1440,224L1440,320L1360,320L1280,320L1200,320L1120,320L1040,320L960,320L880,320L800,320L720,320L640,320L560,320L480,320L400,320L320,320L240,320L160,320L80,320L0,320Z"></path></svg>
      </div>

      <div class="container" id="contebedor-formulario">
        <div id="titulo-formulario" class="text-center mb-4">
          <div> <img src="/Imagenes/undraw_Web_developer_re_h7ie.svg" alt="contacto-formulario" class="img-fluid"></div>
          <h2>Contactos</h2>
          <p class="">Por favor registrate para poder guardarte dentro de nuestros corazones  </p>
        </div>
        <form action="">
          <div class="mb-3">
            <input type="email" class="form-control" id="email" placeholder="name@example.com" required>
          </div>

          <div class="mb-3">
            <input type="text" class="form-control" id="nombre" placeholder="name@example.com" placeholder="Jhonn Colque" required>
          </div>

          <div class="mb-3">
            <input type="telf" class="form-control" id="telf" placeholder="name@example.com" placeholder="604-545-13" required>
          </div>

          <div class="mb-3">
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"
             placeholder="Escribe tu SMS "></textarea>
             <div class="md-3">
               <button type="button" class="btn btn-success w-100 fs-5"> Enviar SMS</button>
             </div>
          </div>
        </form>
      </div>
    </section>

<!-- pie de paguina-->

    <footer class="w-100 d-flex align-items-center justify-content-center flex-wrap">
      <p class="fs-5 px-3 pt-3"> Hotel Dragon Verde &copy; Todos los derechos reservados 2021 Jhonn Derlis 1colque Checchi</p>
      <div id="iconos">
        <a href="#" ><i class="bi bi-facebook"></i></a>
        <a href="#"><i class="bi bi-instagram"></i></a>
        <a href="#"><i class="bi bi-gmail"></i></a>
      </div>
    </footer>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/typewriter-effect@latest/dist/core.js"></script>
    <script src="/JS/main.js"></script>
  </body>
</html>